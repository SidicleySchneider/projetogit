using NUnit.Framework;

namespace ProjetoGit.Tests
{
    public class Tests
    {
        [Test]
        public void TesteValidadorCPF()
        {
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("03529152005"));
        }
        [Test]
        public void TesteValidadorCNPJ()
        {
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("71923975000130"));
        }
    }
}